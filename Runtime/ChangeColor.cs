﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeColor : MonoBehaviour
{
    public GameObject m_target;
    public Material m_targetMaterial;
    public Vector2 m_current;
    public Color[] m_colorRange= new Color[] { Color.red, Color.green, Color.blue};
    void Update()
    {

    }
    private void Reset()
    {
        m_target = gameObject;
        if (m_targetMaterial == null)
            m_targetMaterial = m_target.GetComponent<Renderer>().material;
    }
}
