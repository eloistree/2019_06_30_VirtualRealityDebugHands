﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTexture : MonoBehaviour
{
    public GameObject m_target;
    public float m_speedHorizontal;
    public float m_speedVertical;
    public Material m_targetMaterial;
    public Vector2 m_current;
    void Update()
    {
        if (m_targetMaterial == null)
            m_targetMaterial = m_target.GetComponent<Renderer>().material;
        m_current.x += m_speedHorizontal*Time.deltaTime;
        m_current.y += m_speedVertical * Time.deltaTime;
        m_targetMaterial.SetTextureOffset("_MainTex", m_current);
        
    }
    private void Reset()
    {
        m_target = gameObject;
        if (m_targetMaterial == null)
            m_targetMaterial = m_target.GetComponent<Renderer>().material;
    }
}
